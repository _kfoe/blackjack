# coding=utf-8

import pygame
from util import Util
from mazzo import Mazzo
from player import Player
from dealer import Dealer
from demon import Demon
from loadResource import LoadResource
from fase1 import Fase1
from fase2 import Fase2
from fase3 import Fase3
from fase4 import Fase4

'''
Gestisce tutto il funzionamento
del gioco
'''
class Game(LoadResource):
	'''
	Loop che gestisce il funzionamento del ciclo principale
	del programma.

	Il loop gestisce un subProcess che è eseguito durante
	le varie fasi del gioco.

	Il loop smista il passaggio da una fase all'altra 
	in base alle variabili d'accesso ai loop delle stesse.
	'''
	def loop(self):
		selfInstanceGame = self
		self.mazzo = Mazzo()
		self.player = Player()
		self.dealer = Dealer(self.player, self.mazzo, selfInstanceGame)
		self.subGame = Demon(self.screen, self.player, self.dealer, self.util, self.assets, selfInstanceGame)
		self.fase1 = Fase1(self.screen, self.player, self.dealer, self.util, self.assets, selfInstanceGame)
		self.fase2 = Fase2(self.screen, self.player, self.dealer, self.util, self.assets, selfInstanceGame)
		self.fase3 = Fase3(self.screen, self.player, self.dealer, self.util, self.assets, selfInstanceGame)
		self.fase4 = Fase4(self.screen, self.player, self.dealer, self.util, self.assets, selfInstanceGame)
		
		self.RUN = True
		selfInstanceGame.turnoConcluso = False

		while self.RUN:
			events = pygame.event.get()
			mousePos = pygame.mouse.get_pos()

			self.screen.fill((255,255,255))
			self.screen.blit(self.assets['sfondo'], [0,0])

			self.subGame.loop()

			if self.fase1_START == True:
				self.fase1.loop(mousePos)

			if self.fase2_START == True:
				self.fase2.loop(mousePos)

			if self.fase3_START == True:
				self.fase3.loop(mousePos)

			if self.turnoConcluso == True:
				self.fase4.loopReset(mousePos, events)

			for event in events:
				if event.type == pygame.QUIT:
					self.RUN = False
				elif event.type == pygame.MOUSEBUTTONDOWN:
					if self.fase1_START == True:
						self.fase1_CLICK = True
					elif self.fase2_START == True:
						self.fase2_CLICK = True

			self.clock.tick(60)
			pygame.display.update()