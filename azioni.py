# coding=utf-8

'''
Classe che converte le azioni del gioco a Sprite 
con posizione
'''
class Azioni():
	
	def __init__(self, surface, position):
		self.surface = surface
		self.rect = self.surface.get_rect()
		self.rect.left, self.rect.top = position
		
	def getPos(self):
		return (self.rect.left, self.rect.top)

	def getSurface(self):
		return self.surface