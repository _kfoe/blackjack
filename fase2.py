# coding=utf-8

import pygame
from itertools import repeat

'''
	Gestisce le operazioni: stand, split, hit, double
'''
class Fase2():

	def __init__(self, screen, player, dealer, util, assets, selfInstanceGame):
		self.screen = screen
		self.player = player
		self.dealer = dealer
		self.util  = util
		self.assets = assets
		self.selfInstanceGame = selfInstanceGame

	'''
	Stampa le sprite relative alle azioni disponibili
	in base al turno corrente e al saldo
	'''
	def stampaAzioni(self):
		for c in self.util.getAzioni(self.player):
			currentAction = self.assets['azioni'][c]
			self.screen.blit(currentAction.getSurface(), currentAction.getPos())
	
	'''
	loop di operazioni che viene eseguito
	durante la fase 2.
	'''
	def loop(self, mousePos):
		if self.selfInstanceGame.fase3_START is False and self.selfInstanceGame.turnoConcluso is False:
			self.stampaAzioni()

			if self.selfInstanceGame.fase2_CLICK is True:
				self.check_COLLIDEPOINT_CHOOSE_ACTION(mousePos)
				self.selfInstanceGame.fase2_CLICK = False

	'''
	Gestisco lo switch delle operazioni split, stand, hit, double.

	TODO: Implementare la funzione split.
	'''
	def check_COLLIDEPOINT_CHOOSE_ACTION(self, mousePos):
		for c in ['split', 'stand', 'hit', 'double']:
			currentAction = self.assets['azioni'][c]
			if currentAction.rect.collidepoint(mousePos) == 1 and mousePos != 0:
				if c == "hit":
					self.player.mano.append(self.selfInstanceGame.mazzo.getMazzo().pop(0))
				elif c == "stand":
					self.selfInstanceGame.fase3_START = True
				elif c == "double":
					puntataAttuale = self.player.getPuntata()
					self.player.mano.append(self.selfInstanceGame.mazzo.getMazzo().pop(0))
					self.player.chipsGiocate = [x for item in self.player.chipsGiocate  for x in repeat(item, 2)]
					self.player.setPuntata(puntataAttuale, False)
					self.player.setSaldo(puntataAttuale, False)
					self.selfInstanceGame.fase3_START = True