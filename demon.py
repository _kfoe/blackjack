# coding=utf-8

import pygame
'''
Gestisce le logiche da eseguire durante tutte le fasi del gioco,
personalizzandole in base alla fase corrente.
'''
class Demon():

	def __init__(self, screen, player, dealer, util, assets, selfInstanceGame):
		self.screen = screen
		self.player = player
		self.dealer = dealer
		self.util  = util
		self.assets = assets
		self.selfInstanceGame = selfInstanceGame

	'''
	Stampa la puntata del giocatore
	'''
	def stampaPuntata(self, puntataSurface):
		spritePos = self.util.loadJsonFile("spritePos.json")['numbers'][0]['puntata']
		for idx, s in enumerate(puntataSurface):
			if(idx == 0):
				lastOffset = spritePos['start'][0]
			else:
				lastOffset += spritePos['delta']

			self.screen.blit(s, (lastOffset, spritePos['start'][1] ) )

	'''
	Stampa il saldo del giocatore
	'''
	def stampaSaldo(self, saldoSurface):
		spritePos = self.util.loadJsonFile("spritePos.json")['numbers'][0]['saldo']
		for idx, s in enumerate(saldoSurface):
			if(idx == 0):
				lastOffset = spritePos['start'][0]
			else:
				lastOffset += spritePos['delta']

			self.screen.blit(s, (lastOffset, spritePos['start'][1] ) )
	
	'''
	Durante le varie fasi del gioco sono diversi i messaggi
	che vengono stampati. Questo metodo li gestisce e li stampa.
	'''
	def stampaMessaggiBoard(self):
		'''
		DURANTE FASE 1
		'''
		if self.selfInstanceGame.fase1_START is True:
			if len(self.player.getChipsGiocate()) >= 1:
				self.screen.blit(self.assets['targhette']['deal'].getSurface(), self.assets['targhette']['deal'].getPos())

			self.screen.blit(self.assets['targhette']['place_bet'].getSurface(), self.assets['targhette']['place_bet'].getPos())

		'''
		DURANTE FASE 2
		'''
		if self.selfInstanceGame.fase2_START is True:
			self.screen.blit(self.assets['targhette']['valore_mano'].getSurface(), self.assets['targhette']['valore_mano'].getPos())
			self.screen.blit(self.assets['targhette']['valore_mano'].getSurface(), [435, 0])

			if type(self.util.getValoreMano(self.player.getMano())) is list:
				self.screen.blit(self.assets['targhette']['valore_mano'].getSurface(), [435, 330])
			if type(self.util.getValoreMano(self.dealer.getMano())) is list:
				self.screen.blit(self.assets['targhette']['valore_mano'].getSurface(), [435, 100])

		'''
		DURANTE FASE 4 (sballato, bj e fine partita)
		'''
		if self.selfInstanceGame.turnoConcluso is True:
			for m in self.selfInstanceGame.messaggiBoard:
				self.screen.blit(m.getSurface(), m.getPos())

	'''
	Stampa il valore della mano del dealer e del giocatore,
	include logica presenza asso (1,11) e carta coperta.
	'''
	def stampaValoreMano(self):
		valoreManoDealer = self.util.getValoreMano(self.dealer.getMano())
		valoreManoPlayer = self.util.getValoreMano(self.player.getMano())
		spritePos = self.util.loadJsonFile("spritePos.json")['numbers'][0]['valoreCarte']

		'''
		Dealer
		'''
		if type(valoreManoDealer) is int:
			for idx, num in enumerate(self.util.convertNumberToImage(self.assets['numbers'], valoreManoDealer)):
				self.screen.blit(num, ((spritePos['dealer']['singola'][0] + (spritePos['delta'] * idx)), spritePos['dealer']['singola'][1]))
		elif type(valoreManoDealer) is list:
			for idx, num in enumerate(self.util.convertNumberToImage(self.assets['numbers'], valoreManoDealer[0])):
				self.screen.blit(num, ((spritePos['dealer']['singola'][0] + (spritePos['delta'] * idx)), spritePos['dealer']['singola'][1]))
			
			for idx, num in enumerate(self.util.convertNumberToImage(self.assets['numbers'], valoreManoDealer[1])):
				self.screen.blit(num, ((spritePos['dealer']['doppia'][0] + (spritePos['delta'] * idx)), spritePos['dealer']['doppia'][1]))
		
		'''
		Player
		'''
		if type(valoreManoPlayer) is int:
			for idx, num in enumerate(self.util.convertNumberToImage(self.assets['numbers'], valoreManoPlayer)):
				self.screen.blit(num, ((spritePos['player']['singola'][0] + (spritePos['delta'] * idx)), spritePos['player']['singola'][1]))
		elif type(valoreManoPlayer) is list:
			for idx, num in enumerate(self.util.convertNumberToImage(self.assets['numbers'], valoreManoPlayer[0])):
				self.screen.blit(num, ((spritePos['player']['singola'][0] + (spritePos['delta'] * idx)), spritePos['player']['singola'][1]))
			
			for idx, num in enumerate(self.util.convertNumberToImage(self.assets['numbers'], valoreManoPlayer[1])):
				self.screen.blit(num, ((spritePos['player']['doppia'][0] + (spritePos['delta'] * idx)), spritePos['player']['doppia'][1]))
	
	'''
	Chiama una funzione specifica
	in base al contesto per stampare le carte 
	a schermo.
	'''
	def stampaCarte(self):
		self.player.getManoPrint(self.screen, self.assets)
		self.dealer.getManoPrint(self.screen, self.assets)

	'''
	In base alla fase del gioco, stampa le chips al centro
	oppure sparse nell'apposito spazio
	'''
	def stampaChips(self, chips):
		if self.selfInstanceGame.fase1_START is True:
			for index, c in enumerate(chips):
				currentChips = self.assets['chips'][c]
				self.screen.blit(currentChips.getSurface(), currentChips.getPos())

			if len(self.player.getChipsGiocate()) >= 1:
				self.screen.blit(self.assets['chips']['sprite_centro_chips'].getSurface(),self.assets['chips']['sprite_centro_chips'].getPos())

			spritePos = self.util.loadJsonFile("spritePos.json")['chipsAlCentro']
			for idx, c in enumerate(self.player.chipsGiocate[-5:]):
				if(idx == 0):
					lastOffset = spritePos['start'][0]
				else:
					lastOffset += spritePos['delta'][idx]

				self.screen.blit(c.getSurface(), (lastOffset, spritePos['start'][1] ))
		elif self.selfInstanceGame.fase2_START is True:
			for chips in self.player.getChipsGiocate():
				if chips.getValue() == 5:
					self.screen.blit(pygame.transform.scale(chips.getSurface(), (50, 50)), (183, 578))
				elif chips.getValue() == 10:
					self.screen.blit(pygame.transform.scale(chips.getSurface(), (50, 50)), (183 + 30, 578))
				elif chips.getValue() == 25:
					self.screen.blit(pygame.transform.scale(chips.getSurface(), (50, 50)), (183 + 60, 578))
				elif chips.getValue() == 100:
					self.screen.blit(pygame.transform.scale(chips.getSurface(), (50, 50)), (183 + 90, 578))
	
	'''
	Verifica se dealer o giocatore ha sballato o blackjack.
	Se nessuna delle due si va alla verifica classica 
	con valore carte, include parita
	'''
	def checkSballatoEFinePartita(self):
		valoreManoDealer = self.util.getValoreMano(self.dealer.getMano())
		valoreManoPlayer = self.util.getValoreMano(self.player.getMano())

		'''
		Logiche sballato e blackjack
		'''

		if type(valoreManoPlayer) is list and min(valoreManoPlayer[0], valoreManoPlayer[1]) > 21:
			self.selfInstanceGame.fase4.bustPlayer()
		elif type(valoreManoPlayer) is int and valoreManoPlayer == 21:
			self.selfInstanceGame.fase4.blackjackPlayer()
		elif type(valoreManoPlayer) is int and valoreManoPlayer > 21:
			self.selfInstanceGame.fase4.bustPlayer()

		if type(valoreManoDealer) is list and min(valoreManoDealer[0], valoreManoDealer[1]) > 21:
			self.selfInstanceGame.fase4.bustDealer()
		elif type(valoreManoDealer) is int and valoreManoDealer == 21:
			self.selfInstanceGame.fase4.blackjackDealer()
		elif type(valoreManoDealer) is int and valoreManoDealer > 21:
			self.selfInstanceGame.fase4.bustDealer()

		if self.selfInstanceGame.eseguiCheckFinePartita is True:
			'''
			logiche fine partita
			'''
			if type(valoreManoDealer) is list:
				valoreManoDealer = max(valoreManoDealer[0], valoreManoDealer[1])

			if type(valoreManoPlayer) is list:
				valoreManoPlayer = max(valoreManoPlayer[0], valoreManoPlayer[1])

			if valoreManoPlayer > valoreManoDealer:
				self.selfInstanceGame.fase4.playerWin()
			elif valoreManoPlayer == valoreManoDealer:
				self.selfInstanceGame.fase4.parita()
			elif valoreManoPlayer < valoreManoDealer:
				self.selfInstanceGame.fase4.dealerWin()

			self.selfInstanceGame.turnoConcluso = True

	'''
	loop relativo al subProcess che esegue,
	in base alla fase corrente,
	i metodi descritti in precendenza
	'''
	def loop(self):
		if self.selfInstanceGame.turnoConcluso is False:
			self.checkSballatoEFinePartita()

		self.stampaPuntata(self.util.convertNumberToImage(self.assets['numbers'], self.player.getPuntata()))
		self.stampaSaldo(self.util.convertNumberToImage(self.assets['numbers'], self.player.getSaldo()))
		self.stampaChips(self.util.getChips(self.player.getSaldo()))
		self.stampaMessaggiBoard()
		if self.selfInstanceGame.fase2_START is True:
			self.stampaCarte()
			self.stampaValoreMano()
