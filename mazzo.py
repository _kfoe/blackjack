# coding=utf-8

import random
import itertools
from util import Util

'''
Genera un mazzo e lo mischia
'''
class Mazzo():
	__semi = ['C', 'Q', 'F', 'P']
	__valori = [str(x) for x in range(2,10)] + ["A", "J", "Q", "K"]
	product = [x + "_" + y for x, y in itertools.product(__valori, __semi)] * 4

	def __init__(self):
		self.mazzo = Mazzo.product
		random.shuffle(Mazzo.product)

	def __len__(self):
		return len(self.mazzo)

	def __str__(self):
		return "In questa partita viene usato un solo mazzo per un totale di " + str(self.__len__()) + " carte"

	def getMazzo(self):
		return self.mazzo