# coding=utf-8

'''
Classe che crea un Dealer 
e gestisce le relative operazioni
possibili dallo stesso
'''
from util import Util

class Dealer():

	def __init__(self, giocatore, mazzo, selfInstanceGame):
		self.mano = []
		self.giocatore = giocatore
		self.mazzo = mazzo.getMazzo()
		self.selfInstanceGame = selfInstanceGame
		self.util  = Util()
		
	'''
	Distribuisce le carte all'inizio del turno.
	'''
	def distribuisciCarte(self):
		self.giocatore.mano = [self.mazzo.pop(0), self.mazzo.pop(0)]
		self.mano = [self.mazzo.pop(0), self.mazzo.pop(0)]

	'''
	Aumenta/decrementa il saldo del giocatore
	'''
	def setSaldoGiocatore(self, saldo, isDown = True):
		self.player.setSaldo(saldo, isDown)

	'''
	Inizializza la mano del dealer
	'''
	def reset(self):
		self.mano = []

	'''
	Ritorna le carte del dealer, include logica carta coperta 
	utilizzata anche per il calcolo del valore della mano
	'''
	def getMano(self):
		if self.selfInstanceGame.dealerScopreCarta is True:
			return self.mano
		else:
			return self.mano[:-1]

	'''
	Stampa le carte del dealer, include la logica della carta coperta
	'''
	def getManoPrint(self, screen, assets):
		for idx, c in enumerate(self.getMano()):
			screen.blit(assets['carte'][c].getSurface(),(173 + (30 * idx), 20))

		if len(self.getMano()) == 1:
			screen.blit(assets['carte']['retro_carta'].getSurface(),(173 + 30, 20))


