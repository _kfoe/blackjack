# coding=utf-8

import pygame

'''
	Gestisce la logica del dealer.
'''
class Fase3():

	def __init__(self, screen, player, dealer, util, assets, selfInstanceGame):
		self.screen = screen
		self.player = player
		self.dealer = dealer
		self.util  = util
		self.assets = assets
		self.selfInstanceGame = selfInstanceGame

	'''
	loop di operazioni che viene eseguito
	durante la fase 3.
	'''
	def loop(self, mousePos):
		self.logicaDealer()

	'''
	gestisce la logica del dealer, se pescare o stare.
	'''
	def logicaDealer(self):
		valoreManoDealer = self.util.getValoreMano(self.dealer.getMano())

		if type(valoreManoDealer) is list:
			valoreManoDealer = min(valoreManoDealer[0], valoreManoDealer[1])

		if valoreManoDealer <= 16:
			self.dealer.mano.append(self.dealer.mazzo.pop(0))
		elif valoreManoDealer >= 17:
			self.selfInstanceGame.fase3_START = False
			self.selfInstanceGame.eseguiCheckFinePartita = True