# coding=utf-8

'''
Genera un player con saldo iniziale pari a 3000
'''
class Player():

	def __init__(self):
		self.saldo = 100
		self.puntataTurno = 0
		self.valoreMano = 0
		self.chipsGiocate = []
		self.mano = []
	
	'''
	Aggiorna la puntata del turno per il player
	definito
	'''
	def setPuntata(self, puntataTurno, isDown = True):
		if isDown is True:
			self.puntataTurno -= puntataTurno
		else:
			self.puntataTurno += puntataTurno

	'''
	Ripristina le variabili del player al
	loro valore iniziale per poter passare 
	al turno successivo
	'''
	def reset(self):
		self.puntataTurno = 0
		self.valoreMano = 0
		self.chipsGiocate = []
		self.mano = []

	'''
	Ritorna la puntata corrente del turno
	'''
	def getPuntata(self):
		return self.puntataTurno

	'''
	Aggiorna il saldo per il player definito
	'''
	def setSaldo(self, saldo, isDown = True):
		if isDown is True:
			self.saldo -= saldo
		else:
			self.saldo += saldo

	'''
	Aggiunge una chips a quelle precedentemente
	giocate
	'''
	def setChipsGiocate(self, chips):
		self.chipsGiocate.append(chips)
	
	'''
	ritorna l'elenco di chips giocate
	'''
	def getChipsGiocate(self):
		return self.chipsGiocate

	'''
	ritorna il saldo del giocatore
	'''
	def getSaldo(self):
		return self.saldo
	
	'''
	ritorna la mano del giocatore
	'''
	def getMano(self):
		return self.mano
	
	'''
	stampa a schermo la mano del giocatore
	'''
	def getManoPrint(self, screen, assets):
		for idx, c in enumerate(self.getMano()):
			screen.blit(
				assets['carte'][c].getSurface(),
				(173 + (30 * idx), 380)
				)
	'''
	ritorna il valore della mano del giocatore.
	'''
	def getValoreMano(self):
		return self.valoreMano

