# coding=utf-8

'''
Classe che converte le immagini delle targhette a Sprite 
con posizione.
'''
class Targhette():

	def __init__(self, surface, position):
		self.surface = surface
		self.rect = self.surface.get_rect()
		self.rect.left, self.rect.top = position

	def getPos(self):
		return (self.rect.left, self.rect.top)

	def getSurface(self):
		return self.surface