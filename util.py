# -*- coding: utf-8 -*-
from os import listdir, walk
from os.path import isfile, join
import pygame
import re
import json
from chips import Chips
from targhette import Targhette
from azioni import Azioni
from carte import Carte

'''
Classe di helper per il gioco
Contiene metodi per gestire le sprite
e le logiche di controllo sul turno
'''
class Util():

	def __init__(self):
		pass

	'''
	Carica l'assets generico che non ha posizionamenti calcolati precedentemente.
	'''
	def loadGenericAssets(self):
		assets = {}

		for currentFile in listdir("assets/generico"):
			if (isfile(join("assets/generico", currentFile)) and re.search(re.compile(r"(.png|.jpg)+"), currentFile)):
				assets[currentFile.split('.')[0]] = pygame.image.load(join("assets/generico", currentFile), currentFile)
		return assets      

	'''
	Carica l'assets relativo alle chips convertendole in sprite
	'''
	def loadChips(self):
		chips = {}

		spritePos = self.loadJsonFile("spritePos.json")['chips']

		for idx, currentFile in enumerate(listdir("assets/chips")):
			if (isfile(join("assets/chips", currentFile)) and re.search(re.compile(r"(.png|.jpg)+"), currentFile)):
				
				if(idx == 0):
					lastOffset = spritePos['start'][0]
				else:
					lastOffset += spritePos['delta']

				chips[currentFile.split('.')[0]] = Chips(
					pygame.image.load(join("assets/chips", currentFile), currentFile),
					(203,484) if (currentFile.split('.')[0] == "sprite_centro_chips") else (lastOffset, spritePos['start'][1]),
					0 if (currentFile.split('.')[0] == "sprite_centro_chips") else spritePos['values'][idx]
					)
				
		return chips    

	'''
	Carica l'assets relativo alle targhette convertendole in sprite
	'''
	def loadTar(self):
		targhette = {}
		spritePos = self.loadJsonFile("spritePos.json")['targhette']
		for idx, currentFile in enumerate(listdir("assets/targhette")):
			if (isfile(join("assets/targhette", currentFile)) and re.search(re.compile(r"(.png|.jpg)+"), currentFile)):

				targhette[currentFile.split('.')[0]] = Targhette(
					pygame.image.load(join("assets/targhette", currentFile), currentFile),
					spritePos[idx]['pos']
					)
				
		return targhette      

	'''
	Carica l'assets relativo alle azioni convertendole in sprite
	'''
	def loadAzioni(self):
		azioni = {}
		spritePos = self.loadJsonFile("spritePos.json")['azioni']
		for idx, currentFile in enumerate(listdir("assets/azioni"), -1):
			if (isfile(join("assets/azioni", currentFile)) and re.search(re.compile(r"(.png|.jpg)+"), currentFile)):
				azioni[currentFile.split('.')[0]] = Azioni(
					pygame.image.load(join("assets/azioni", currentFile), currentFile),
					spritePos[idx]['pos']
					)
				
		return azioni      

	'''
	Carica l'assets relativo alle carte convertendole in sprite
	'''
	def loadCards(self):
		carte = {}
		for currentFile in listdir("assets/carte"):
			if (isfile(join("assets/carte", currentFile)) and re.search(re.compile(r"(.png|.jpg)+"), currentFile)):
				carte[currentFile.split('.')[0]] = Carte(
					pygame.image.load(join("assets/carte", currentFile), currentFile),
					self.getValueCard(currentFile.split('.')[0])
					)
				
		return carte    

	'''
	Carico l'assets che andrà a formare i numeri 
	per la puntata e per il saldo e altro
	'''
	def loadNumbers(self):
		numbers = {}
		for currentFile in listdir("assets/numeri"):
			if (isfile(join("assets/numeri", currentFile)) and re.search(re.compile(r"(.png|.jpg)+"), currentFile)):
				numbers[currentFile.split('.')[0]] = pygame.image.load(join("assets/numeri", currentFile), currentFile)
		return numbers      
	
	'''
	Parsa il contenuto di un file json
	così da poter essere utilizzato
	'''
	def loadJsonFile(self, jsonFile):
		with open(jsonFile) as jsonData:
			data = json.load(jsonData)
		return data 
	
	'''
	Converte un numero in una serie di Sprite (numeri)
	'''
	def convertNumberToImage(self, assets, number):
		numbers = []
		for n in str(number):
			if n != '-':
				numbers.append(assets[n])
		return numbers

	'''
	Ritorna le chips disponibili in base al saldo
	disponibile dal giocatore
	'''
	def getChips(self, saldo):
		chips = ["chips_0" + str(x) for x in range(0,4)]

		if(saldo >= 100):
			return chips
		elif (saldo < 100 and saldo >= 25):
			return chips[:3]
		elif (saldo < 25 and saldo >= 10):
			return chips[:2]
		elif (saldo < 10 and saldo >= 5):
			return chips[:1]
		else:
			return []

	'''
	Ritorna le azioni diponibili in base 
	al saldo, al numero di carte e al loro
	valore.

	TODO: implementa split.
	'''
	def getAzioni(self, player):
		manoParsata = self.parseCard(player.getMano())
		azioni = ['double', 'hit', 'stand'] #'split', 'stand']

		if player.getSaldo() == 0:
			return ['hit', 'stand'] #'split']
		#todo: fix (caso split se hai un lettera (J) e il valore 10).
		elif len(player.getMano()) > 2 or ((len(player.getMano()) == 2) and manoParsata[0] != manoParsata[1]):
			return ['hit', 'stand']
		else:
			return azioni

	'''
	Ciascuna carta viene registra con il seguente pattern VALORE_SEME - 
	Questo metodo parse le carte considerando solo il valore
	'''
 	def parseCard(self, cards):
 		return [s.split('_')[0] for s in cards]
 	
 	'''
 	Ritorna il valore della carta
 	'''
	def getValueCard(self, card):
		card = card.split('_')[0]
		if card in ['J', 'Q', 'K']:
			return 10
		elif card in ['A']:
			return [1, 11]
		elif card == "retro":
			return 0
		else:
			return int(card)

	'''
	Ritorna il valore della mano,
	include logica presenza di assi, quindi con doppio valore
	'''
	def getValoreMano(self, mano):
		controllaAllaFine = []
		valoreMano = 0
		valueCard = 0
		assi = 0
		for a in mano:
			valueCard = self.getValueCard(a)

			if type(valueCard) is list and len(valueCard) == 2:
				assi += 1
			else:
				valoreMano += valueCard

		if assi == 1:
			if valoreMano + 11 == 21:
				valoreMano += 11
			elif valoreMano + 11 < 21:
				valoreMano = [(valoreMano + 1), (valoreMano + 11)]
			else:
				valoreMano += 1
		elif assi >= 2:
			valoreMano += assi

		return valoreMano