# coding=utf-8

'''
Classe che converte le immagini delle carte a Sprite 
con posizione e valore.
'''
class Carte():

	def __init__(self, surface, value):
		self.surface = surface
		self.value = value

	def getValue(self):
		return self.value

	def getSurface(self):
		return self.surface