# coding=utf-8

import pygame

'''
Gestisce gli esiti della partita
'''
class Fase4():

	def __init__(self, screen, player, dealer, util, assets, selfInstanceGame):
		self.screen = screen
		self.player = player
		self.dealer = dealer
		self.util  = util
		self.assets = assets
		self.selfInstanceGame = selfInstanceGame
	
	'''
	Player sballa
	'''
	def bustPlayer(self):
		self.selfInstanceGame.turnoConcluso = True
		self.selfInstanceGame.messaggiBoard.append(self.assets['targhette']['player_bust'])
	
	'''
	Dealer sballa
	'''
	def bustDealer(self):
		self.selfInstanceGame.turnoConcluso = True
		self.selfInstanceGame.messaggiBoard.append(self.assets['targhette']['dealer_bust'])
		self.player.setSaldo((self.player.getPuntata() * 2))
	
	'''
	Player fa blackjack
	'''
	def blackjackPlayer(self):
		self.selfInstanceGame.turnoConcluso = True
		self.selfInstanceGame.messaggiBoard.append(self.assets['targhette']['player_win_bj'])
		self.player.setSaldo(((self.player.getPuntata() * 2) + 5))
	
	'''
	Dealer fa blackjack
	'''
	def blackjackDealer(self):
		self.selfInstanceGame.turnoConcluso = True
		self.selfInstanceGame.messaggiBoard.append(self.assets['targhette']['dealer_win'])
	
	'''
	Player vince perché ha valore più alto
	rispetto alle carte del dealer.
	'''
	def playerWin(self):
		self.selfInstanceGame.turnoConcluso = True
		self.selfInstanceGame.messaggiBoard.append(self.assets['targhette']['player_win'])
		self.player.setSaldo((self.player.getPuntata() * 2))
	
	'''
	Il contrario di playerWin.
	'''
	def dealerWin(self):
		self.selfInstanceGame.turnoConcluso = True
		self.selfInstanceGame.messaggiBoard.append(self.assets['targhette']['dealer_win'])

	'''
	Player e Dealer pareggiano.
	'''
	def parita(self):
		self.selfInstanceGame.turnoConcluso = True
		self.selfInstanceGame.messaggiBoard.append(self.assets['targhette']['parita'])
		self.player.setSaldo(self.player.getPuntata())

	'''
	loop di operazioni che viene eseguito
	per le variabili per turno a seguito della
	conclusione dello stesso

	Se il saldo termina, viene ricaricato a seguito del nuovo turno.
	'''
	def loopReset(self, mousePos, events):
		if self.selfInstanceGame.turnoConcluso is True:
			self.selfInstanceGame.messaggiBoard.append(self.assets['targhette']['restart_play'])

			if(self.player.getSaldo() == 0):
				self.selfInstanceGame.messaggiBoard.append(self.assets['targhette']['ricarica'])
				self.player.saldo = 1000

			for event in events:
				if event.type == pygame.MOUSEBUTTONDOWN and self.assets['targhette']['restart_play'].rect.collidepoint(mousePos) and mousePos != 0:
					self.selfInstanceGame.messaggiBoard = []
					self.selfInstanceGame.fase0_START = False
					self.selfInstanceGame.fase0_CLICK = False

					self.selfInstanceGame.fase1_START = True
					self.selfInstanceGame.fase1_CLICK = False
					
					self.selfInstanceGame.fase2_START = False
					self.selfInstanceGame.fase2_CLICK = False

					self.selfInstanceGame.fase3_START = False
					self.selfInstanceGame.fase3_CLICK = False

					self.selfInstanceGame.dealerScopreCarta = False
					self.selfInstanceGame.turnoConcluso = False
					self.selfInstanceGame.eseguiCheckFinePartita = False
					self.player.reset()
					self.dealer.reset()