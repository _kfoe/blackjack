# coding=utf-8

import pygame
from mazzo import Mazzo
from game import Game

from util import Util

def main():
	pygame.init()
	screen = pygame.display.set_mode([533,800])
	clock = pygame.time.Clock()
	pygame.display.set_caption("Blackjack con Python")
	Game(screen, clock).loop()
	pygame.quit()

if __name__ == '__main__':
	main()