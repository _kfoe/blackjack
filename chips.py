# coding=utf-8

'''
Classe che converte le chips disponibili del gioco a Sprite 
con posizione e rispettivo valore
'''
class Chips():

	def __init__(self, surface, position, value):
		self.surface = surface
		self.rect = self.surface.get_rect()
		self.rect.left, self.rect.top = position
		self.value = value

	def getValue(self):
		return int(self.value)

	def getPos(self):
		return (self.rect.left, self.rect.top)

	def getSurface(self):
		return self.surface