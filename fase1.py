# coding=utf-8

import time
import pygame

'''
	Gestisce le operazioni: avvia turno aumenta/scala puntata
'''
class Fase1():

	def __init__(self, screen, player, dealer, util, assets, selfInstanceGame):
		self.screen = screen
		self.player = player
		self.dealer = dealer
		self.util  = util
		self.assets = assets
		self.selfInstanceGame = selfInstanceGame
	
	'''
	loop di operazioni che viene eseguito
	durante la fase 1.
	'''
	def loop(self, mousePos):
		if self.selfInstanceGame.fase1_CLICK is True:
			self.check_COLLIDEPOINT_CHIPS_START(mousePos)
			self.check_COLLIDEPOINT_CHIPS_ARRIVED(mousePos)
			self.check_COLLIDEPOINT_GO_TO_FASE2(mousePos)
			self.selfInstanceGame.fase1_CLICK = False
	
	'''
	Aumento la puntata aggiungendo chips.
	'''
	def check_COLLIDEPOINT_CHIPS_START(self, mousePos):
		for chips in self.util.getChips(self.player.getSaldo()):
			currentChips = self.assets['chips'][chips]
			if currentChips.rect.collidepoint(mousePos) == 1 and mousePos != 0:
				self.player.setChipsGiocate(self.assets['chips'][chips])
				self.player.setPuntata(currentChips.getValue())
				self.player.setSaldo(currentChips.getValue())

	'''
	Decremento la puntata rimuovendo chips.
	'''
	def check_COLLIDEPOINT_CHIPS_ARRIVED(self, mousePos):
		if self.assets['chips']['sprite_centro_chips'].rect.collidepoint(mousePos) == 1 and mousePos != 0 and len(self.player.chipsGiocate) >= 1:
			deleteChips = self.player.chipsGiocate.pop()
			self.player.setSaldo(deleteChips.getValue(), False)
			self.player.setPuntata(deleteChips.getValue(), False)

	'''
	Passo alla fase 2
	'''
	def check_COLLIDEPOINT_GO_TO_FASE2(self, mousePos):
		if self.assets['targhette']['deal'].rect.collidepoint(mousePos) == 1 and mousePos != 0 and len(self.player.chipsGiocate) >= 1:
			self.selfInstanceGame.fase1_CLICK = False
			self.selfInstanceGame.fase1_START = False
			self.selfInstanceGame.fase2_START = True
			self.dealer.distribuisciCarte()

