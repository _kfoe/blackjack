# coding=utf-8

from util import Util

'''
Carica le risorse necessarie al gioco
durante tutte le fasi
'''
class LoadResource():

	def __init__(self, screen, clock):
		self.screen = screen
		self.clock = clock
		self.util = Util()
		self.messaggiBoard = []
		self.fase0_START = False
		self.fase0_CLICK = False

		self.fase1_START = True
		self.fase1_CLICK = False
		
		self.fase2_START = False
		self.fase2_CLICK = False

		self.fase3_START = False
		self.fase3_CLICK = False

		self.dealerScopreCarta = False
		self.turnoConcluso = False
		self.eseguiCheckFinePartita = False

		self.assets = {
			'sfondo': self.util.loadGenericAssets()['sfondo'],
			'chips': self.util.loadChips(),
			'numbers': self.util.loadNumbers(),
			'targhette': self.util.loadTar(),
			'azioni': self.util.loadAzioni(),
			'carte': self.util.loadCards()
		}
		

